/**
 * life - Universe.java 15/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.models;

/**
 * An universe filled with cells alive or death
 *
 */
public class Universe {
    private CellState[][] universe;
    private int width;
    private int height;
    private int generation = 0;
    private Cell behavior;

    public Universe(int width, int height, Cell behavior) {
        universe = new CellState [width][height];
        this.behavior = behavior;
        this.width = width;
        this.height = height;
    }

    public void nextGeneration() {
        CellState[][] nextUniverse = new CellState[width][height];
        for (int i = 0; i < getWidth(); i++) {
            for (int j = 0; j < getHeight(); j++) {
                nextUniverse[i][j] = getBehavior().step(this, getCell(i, j), i, j);
            }
        }
        universe = nextUniverse;
    }

    /**
     * Get current generation
     */
    public int getGeneration() {
        return this.generation;
    }

    /**
     * Get width
     */
    public int getWidth() {
        return this.width;
    }

    /**
     * Get height
     */
    public int getHeight() {
        return this.height;
    }

    /**
     * Get a cell state from universe
     */
    public CellState getCell(int x, int y) {
        if (x < getWidth() && y < getHeight() && x > 0 && y > 0) {
            return universe[x][y];
        }
        return CellState.CELL_DEAD;
    }

    /**
     * Set cell state
     */
    public Universe setCell(int x, int y, CellState cellState) {
        universe[x][y] = cellState;
        return this;
    }

    public Universe toggleCell(int x, int y) {
        if (CellState.CELL_ALIVE == getCell(x, y)) {
            setCell(x, y, CellState.CELL_DEAD);
        }
        else {
            setCell(x, y, CellState.CELL_ALIVE);
        }
        return this;
    }

    public void clearCells() {
        CellState[][] nextUniverse = new CellState[width][height];
        universe = nextUniverse;
    }

    /**
     * Get Cell behavior
     */
    public Cell getBehavior() {
        return this.behavior;
    }

    /**
     * Set a new Cell behavior
     * @param behavior cell behavior
     * @return this
     */
    public Universe setBehavior(Cell behavior) {
        this.behavior = behavior;
        return this;
    }
}
