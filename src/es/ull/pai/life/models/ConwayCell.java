/**
 * life - ConwayCell.java 15/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.models;

import es.ull.pai.life.utils.Tuple;

/**
 * TODO: Commenta algo
 *
 */
public class ConwayCell extends Cell {

    public ConwayCell() {
    }

    @Override
    public CellState step(Universe universe, CellState cellState, int x, int y) {

        int neighbours = countAliveNeighbours(universe, x, y);
        if (CellState.CELL_ALIVE == cellState) {
            if (neighbours < 2) {
                return CellState.CELL_DEAD;
            } else if (neighbours == 2 || neighbours == 3) {
                return CellState.CELL_ALIVE;
            } else {
                return CellState.CELL_DEAD;
            }
        }
        else if (neighbours == 3){
            return CellState.CELL_ALIVE;
        }
        else {
            return cellState;
        }
    }
}
