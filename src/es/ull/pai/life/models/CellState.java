/**
 * life - CellState.java 15/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.models;

/**
 * Represents two possible states to cell, alive or death
 *
 */
public enum CellState {
    CELL_DEAD,
    CELL_ALIVE
}
