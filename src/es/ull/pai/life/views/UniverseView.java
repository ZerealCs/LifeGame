/**
 * life - UniverseView.java 15/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.views;

import es.ull.pai.life.models.CellState;
import es.ull.pai.life.models.ConwayCell;
import es.ull.pai.life.models.Universe;

import java.awt.*;

/**
 * Shows a universe
 *
 */
public class UniverseView extends DrawerComponent {
    private final int WIDTH_UNIVERSE = 100;
    private final int HEIGHT_UNIVERSE = 100;

    // Attributes
    private Color colorCell = Color.BLACK;
    private Universe model;

    public UniverseView() {
        model = new Universe(WIDTH_UNIVERSE, HEIGHT_UNIVERSE, new ConwayCell());
    }

    @Override
    void draw(Graphics2D graphics2D) {
        final double SIZE_CELL = getSizeCell();
        final int SIZE_WIDTH = (int)(SIZE_CELL * getModel().getWidth());
        final int SIZE_HEIGHT = (int)(SIZE_CELL * getModel().getHeight());


        graphics2D.translate(getPosComponent().getX(), getPosComponent().getY());
        graphics2D.setColor(Color.BLACK);
        for (int i = 0; i <= getModel().getWidth(); i++) {
            graphics2D.drawLine((int)(i * SIZE_CELL), 0, (int)(i * SIZE_CELL), SIZE_HEIGHT);

        }

        for (int i = 0; i <= getModel().getHeight(); i++) {
            graphics2D.drawLine(0, (int)(i * SIZE_CELL), SIZE_WIDTH, (int)(i * SIZE_CELL));
        }

        graphics2D.setColor(getColorCell());
        for (int i = 0; i < getModel().getWidth(); i++) {
            for (int j = 0; j < getModel().getHeight(); j++) {
                int xPos = (int) (i * SIZE_CELL);
                int yPos = (int) (j * SIZE_CELL);
                if (CellState.CELL_ALIVE == getModel().getCell(i, j)) {
                    graphics2D.fillRect(xPos, yPos, (int) Math.ceil(SIZE_CELL), (int) Math.ceil(SIZE_CELL));
                }
            }
        }
    }

    public double getSizeCell() {
        double xRel = getSizeComponent().getX() / getModel().getWidth();
        double yRel = getSizeComponent().getY() / getModel().getHeight();
        return xRel > yRel ? yRel : xRel;
    }

    /**
     * Toggle a cell in function of mouse position
     * @param x position of mouse
     * @param y position of mouse
     */
    public void toggleCell(int x, int y) {
        int xRel = (int)((x - getPosComponent().getX().intValue()) / getSizeCell());
        int yRel = (int)((y - getPosComponent().getY().intValue()) / getSizeCell());

        if (xRel >= 0 || yRel >= 0) {
            getModel().toggleCell(xRel, yRel);
        }
    }

    /**
     * Get model an universe
     */
    public Universe getModel() {
        return this.model;
    }

    /**
     * Set model an universe
     * @param model
     * @return
     */
    public UniverseView setModel(Universe model) {
        this.model = model;
        return this;
    }

    /**
     * Get Color of cells
     */
    public Color getColorCell() {
        return this.colorCell;
    }

    /**
     * Set colorCell of cells
     * @param colorCell
     * @return
     */
    public UniverseView setColorCell(Color colorCell) {
        this.colorCell = colorCell;
        return this;
    }
}
