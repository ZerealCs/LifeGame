/**
 * BallsGame - AppView.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.views;

import es.ull.pai.life.Resources;
import es.ull.pai.life.models.ConwayCell;
import es.ull.pai.life.models.Universe;
import es.ull.pai.life.reactive.Behavior;
import es.ull.pai.life.utils.Tuple;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Optional;

/**
 * Join all views into this class.
 */
public class AppView extends JPanel {
    // Constants
    private final Color BACKGROUND_COLOR = Color.decode("#b6fbff");
    private final int FPS = 1000 / 60;

    // Attributes / variables
    private Optional<Timer> timer = Optional.empty();
    private Canvas canvas = new Canvas();
    private UniverseView universeView = new UniverseView();
    private ControlsView controlsView = new ControlsView();

    // Events/Behaviors
    private Behavior<Double> time = new Behavior<>(0.0);

    /**
     * Make a view
     */
    public AppView() {
        // Setting variables
        setLayout(new BorderLayout());
        add(getCanvas(), BorderLayout.CENTER);
        add(getControlsView(), BorderLayout.NORTH);

        setUpControls();
        setUpCanvas();
        setPreferredSize(new Dimension(500,500));
    }


    public void setUpControls() {
        final int SECOND = 1000; // A second in millis

        getControlsView().getStepBtn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                getUniverseView().getModel().nextGeneration();
                getCanvas().repaint();
            }
        });

        getControlsView().getStartBtn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (!timer.isPresent()) {
                    Timer aux = new Timer(SECOND / getControlsView().getSpeedSld().getValue(), actionEvent ->
                        step()
                    );
                    aux.start();
                    setTimer(Optional.of(aux));
                }
                else {
                    getTimer().ifPresent(timer1 ->
                        timer1.start()
                    );
                }
            }
        });

        getControlsView().getStopBtn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                getTimer().ifPresent(timer1 -> timer1.stop());
            }
        });

        getControlsView().getClearBtn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                getUniverseView().getModel().clearCells();
                getCanvas().repaint();
            }
        });

        getControlsView().getSpeedSld().addChangeListener(changeEvent ->
            getTimer().ifPresent(timer1 -> timer1.setDelay(((JSlider)changeEvent.getSource()).getValue()))
        );
    }

    public void step() {
        getUniverseView().getModel().nextGeneration();
        getCanvas().repaint();
    }

    /**
     * Set differents shapes to draw over canvas
     */
    public void setUpCanvas() {
        getCanvas().getClick().onDispatch(pos -> {
            getUniverseView().toggleCell(pos.getX(), pos.getY());
        });

        getCanvas().getDimension().updates()
                .onDispatch(size -> {
                    getUniverseView().setSizeComponent(new Tuple<>(new Double(size.getX()), new Double(size.getY())));
                    if (size.getY() > size.getX()) {
                        getUniverseView().setPosComponent(new Tuple<>(0.0, (size.getY() - size.getX()) / 2.0));
                    }
                    else {
                        getUniverseView().setPosComponent(new Tuple<>((size.getX() - size.getY()) / 2.0, 0.0));
                    }
                });

        getCanvas().getPaint()
                .onDispatch(g -> {
                    getUniverseView().draw(g);
                });
    }

    /**
     * Get Canvas
     */
    public Canvas getCanvas() {
        return canvas;
    }

    /**
     * Set Canvas
     * @param canvas
     * @return
     */
    public AppView setCanvas(Canvas canvas) {
        this.canvas = canvas;
        return this;
    }

    /**
     * Get Time
     */
    public Behavior<Double> getTime() {
        return time;
    }

    /**
     * Se time
     * @param time
     * @return
     */
    public AppView setTime(Behavior<Double> time) {
        this.time = time;
        return this;
   }
    /**
     * Get timer
     */
    public Optional<Timer> getTimer() {
        return timer;
    }

    /**
     * Set timer
     * @param timer
     * @return
     */
    public AppView setTimer(Optional<Timer> timer) {
        this.timer = timer;
        return this;
    }

    /**
     * get Universe View
     */
    public UniverseView getUniverseView() {
        return this.universeView;
    }

    /**
     * Set Universe View
     * @param universeView
     * @return
     */
    public AppView setUniverseView(UniverseView universeView) {
        this.universeView = universeView;
        return this;
    }

    /**
     *  Get controls
     */
    public ControlsView getControlsView() {
        return controlsView;
    }

    /**
     * Set a new controls buttons
     * @param controlsView
     * @return
     */
    public AppView setControlsView(ControlsView controlsView) {
        this.controlsView = controlsView;
        return this;
    }
}
