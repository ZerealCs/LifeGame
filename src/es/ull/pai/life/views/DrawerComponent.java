/**
 * BallsGame - DrawerComponent.java 7/05/16
 * <p>
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */

package es.ull.pai.life.views;

import es.ull.pai.life.utils.Tuple;

import java.awt.*;

/**
 * By default all components that there are "drawer". This drawer are used to draw over determinate canvas(Graphics)
 * from a JPanel.
 */
abstract class DrawerComponent {
    // Helper Constants to draw in 1 unit
    protected final Tuple<Double> SIZE = new Tuple<>(1.0, 1.0);
    protected final Tuple<Double> SIZE_MIDDLE = new Tuple<>(SIZE.getX() / 2, SIZE.getY() / 2);
    // Attributes
    protected Tuple<Double> sizeComponent = new Tuple<>(100.0, 100.0);      // Size in pixels preferred
    protected Tuple<Double> posComponent = new Tuple<>(0.0, 0.0);           // Pos in pixels preferred

    /**
     * How is draw the shape is defined here override it
     * @param graphics2D
     */
    abstract void draw(Graphics2D graphics2D);

    /**
     * Get size of component
     */
    public Tuple<Double> getSizeComponent() {
        return this.sizeComponent;
    }

    /**
     * Set size of component
     * @param sizeComponent
     * @return
     */
    public DrawerComponent setSizeComponent(Tuple<Double> sizeComponent) {
        this.sizeComponent = sizeComponent;
        return this;
    }

    /**
     * Get position of component
     */
    public Tuple<Double> getPosComponent() {
        return this.posComponent;
    }

    /**
     * Set position of component
     * @param posComponent
     * @return
     */
    public DrawerComponent setPosComponent(Tuple<Double> posComponent) {
        this.posComponent = posComponent;
        return this;
    }
}
