/**
 * BallsGame - Tuple.java May 2, 2016
 * 
 * Copyright 20XX Eleazar Díaz Delgado. All rights reserved.
 */
package es.ull.pai.life.utils;

/**
 * Represents a point. (More specific a generic tuple)
 * @author eleazardd
 *
 */
public class Tuple<X> {
    private X x;
    private X y;
    
    public Tuple(X x, X y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return the x
     */
    public X getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public Tuple<X> setX(X x) {
        this.x = x;
        return this;
    }

    /**
     * @return the y
     */
    public X getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public Tuple setY(X y) {
        this.y = y;
        return this;
    }


}
